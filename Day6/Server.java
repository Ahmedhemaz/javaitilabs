import java.io.DataInputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Vector;

public class Server
{
    ServerSocket serverSocket;
    public Server()
    {
        try {
            serverSocket = new ServerSocket(5005);
        } catch (IOException e) {
            e.printStackTrace();
        }
        while(true)
        {
            Socket s = null;
            try {
                s = serverSocket.accept();
            } catch (IOException e) {
                e.printStackTrace();
            }
            new ChatHandler(s);
        }
    }
    public static void main(String[] args)
    {
        new Server();
    }
}
class ChatHandler extends Thread
{
    DataInputStream dis;
    PrintStream ps;
    static Vector<ChatHandler> clientsVector =
            new Vector<ChatHandler>();
    public ChatHandler(Socket cs)
    {
        try {
            dis = new DataInputStream(cs.getInputStream());
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            ps = new PrintStream(cs.getOutputStream());
        } catch (IOException e) {
            e.printStackTrace();
        }
        clientsVector.add(this);
        start();
    }
    public void run()
    {
        while(true)
        {
            String str = null;
            try {
                str = dis.readLine();
                if(str == null){
                    break;
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            System.out.println(str);
            sendMessageToAll(str);
        }
    }
    void sendMessageToAll(String msg)
    {
        for(ChatHandler ch : clientsVector)
        {
            ch.ps.println(msg);
        }
    }
}