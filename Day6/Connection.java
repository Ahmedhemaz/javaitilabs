import java.io.DataInputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.net.Socket;

public class Connection {
    Socket mySocket;
    DataInputStream dis ;
    PrintStream ps;

    public Connection(String ip, int portNumber) {
        try{
            this.mySocket = new Socket(ip, portNumber);
            this.dis =  new DataInputStream(mySocket.getInputStream ());
            this.ps = new PrintStream(mySocket.getOutputStream ());
        } catch(IOException ex)
        {
            ex.printStackTrace();
        }

    }
}
