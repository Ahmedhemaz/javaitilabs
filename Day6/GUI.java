import java.awt.*;
import java.awt.event.*;
import java.io.IOException;
import java.util.Scanner;
import javax.swing.*;
public class GUI extends JFrame  implements Runnable {
    Thread th;
    Connection connection;
    JTextField tf;
    JTextArea ta;
    public GUI(String ip, int port){
        this.connection = new Connection(ip,port);
        this.setLayout(new FlowLayout());
        this.ta = new JTextArea(20,50);
        JScrollPane scroll = new JScrollPane(ta);
        this.tf = new JTextField(40);
        JButton okButton = new JButton("Send");
        th = new Thread(this);
        th.start();
        okButton.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent ae){
                //ta.append(tf.getText()+"\n");
                connection.ps.println(tf.getText());
                tf.setText("");
            }
        });
        add(scroll);
        add(tf);
        add(okButton);
    }

    @Override
    public void run() {
        while (true){
            String str;
            try {
                str = connection.dis.readLine();
                if(str !=null){
                    ta.append(str+"\n");
                }else{
                    break;
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }

    public static void main(String args[])
    {
        GUI ui=new GUI("127.0.0.1",5005);
        GUI ui2=new GUI("127.0.0.1",5005);

        ui.setSize(600, 400);
        ui.setResizable(false);
        ui.setVisible(true);
    }
}