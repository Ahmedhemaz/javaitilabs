import java.io.*;
import java.util.StringTokenizer;

public class PrintIp {

    public static void main(String[] args) {
        splitIp(args[0]);
        tokenizeIp(args[0]);
        subStringIp(args[0]);
    }

    public static void splitIp(String ip){
        for (String part : ip.split("\\.")){
            System.out.println(part);
        }

    }

    public static void  tokenizeIp(String Ip){
        StringTokenizer token = new StringTokenizer(Ip,".");
        while(token.hasMoreTokens()){
            System.out.println(token.nextToken());
        }
    }

         public static void subStringIp(String ip){
        String tempString ;
        int startOfString =0;
        for (int i=0; i<5; i++){
            int index = ip.indexOf('.',startOfString);
            if(index >= ip.length()){
                tempString = ip.substring(startOfString,ip.lastIndexOf('.'));
            }else{
                tempString = ip.substring(startOfString,index);
            }
            System.out.println(tempString);
            startOfString = index+1;

        }

    }
}
