package sample;

import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TextArea;
import javafx.scene.layout.BorderPane;
import javafx.stage.FileChooser;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Scanner;

class SampleController extends BorderPane {

    protected final MenuBar menuBar;
    protected final Menu menu;
    protected final MenuItem menuItem;
    protected final MenuItem menuItem0;
    protected final MenuItem menuItem1;
    protected final MenuItem menuItem2;
    protected final Menu menu0;
    protected final MenuItem menuItem3;
    protected final MenuItem menuItem4;
    protected final MenuItem menuItem5;
    protected final MenuItem menuItem6;
    protected final MenuItem menuItem7;
    protected final MenuItem menuItem8;
    protected final Menu menu1;
    protected final MenuItem menuItem9;
    protected final TextArea textArea;


    public SampleController() {

        menuBar = new MenuBar();
        menu = new Menu();
        menuItem = new MenuItem();
        menuItem0 = new MenuItem();
        menuItem1 = new MenuItem();
        menuItem2 = new MenuItem();
        menu0 = new Menu();
        menuItem3 = new MenuItem();
        menuItem4 = new MenuItem();
        menuItem5 = new MenuItem();
        menuItem6 = new MenuItem();
        menuItem7 = new MenuItem();
        menuItem8 = new MenuItem();
        menu1 = new Menu();
        menuItem9 = new MenuItem();
        textArea = new TextArea();

        setMaxHeight(USE_PREF_SIZE);
        setMaxWidth(USE_PREF_SIZE);
        setMinHeight(USE_PREF_SIZE);
        setMinWidth(USE_PREF_SIZE);
        setPrefHeight(465.0);
        setPrefWidth(678.0);

        BorderPane.setAlignment(menuBar, javafx.geometry.Pos.CENTER);

        menu.setMnemonicParsing(false);
        menu.setText("File");

        menuItem.setMnemonicParsing(false);
        menuItem.setText("New");
        menuItem.setOnAction(event ->{
            textArea.deleteText(0,textArea.getLength());
            System.out.println(event.toString());
        });

        menuItem0.setMnemonicParsing(false);
        menuItem0.setText("Open");
        menuItem0.setOnAction(event -> {
            System.out.println(event.toString());
            FileChooser fileChooser= new FileChooser();
            fileChooser.getExtensionFilters().addAll(
                    new FileChooser.ExtensionFilter("All Files", "*.*"));
            File selectedFile = fileChooser.showOpenDialog(null);
            File file = new File(selectedFile.toString());
            try {
                Scanner sc = new Scanner(file);
                while (sc.hasNext()){
                    textArea.appendText(sc.nextLine() + "\n");
                }
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
            System.out.println(selectedFile);

        });

        menuItem1.setMnemonicParsing(false);
        menuItem1.setText("Save");
        menuItem1.setOnAction(event -> {
            System.out.println(event.toString());
            FileChooser fileChooser = new FileChooser();
            fileChooser.getExtensionFilters().addAll(
                    new FileChooser.ExtensionFilter("Text Files", "*.txt"),
                    new FileChooser.ExtensionFilter("Image Files", "*.png", "*.jpg", "*.gif"),
                    new FileChooser.ExtensionFilter("Audio Files", "*.wav", "*.mp3", "*.aac"),
                    new FileChooser.ExtensionFilter("HTML Files", "*.html"),
                    new FileChooser.ExtensionFilter("Java Files", "*.java"));
            File selectedFile = fileChooser.showSaveDialog(null);
            String fileContent = textArea.getText();
            try{
                Files.write(selectedFile.toPath(),fileContent.getBytes());
            }catch (IOException e){
                e.printStackTrace();
            }

        });

        menuItem2.setMnemonicParsing(false);
        menuItem2.setText("Exit");
        menuItem2.setOnAction(event ->{
            System.out.println(event.toString());
            System.exit(0);
        });

        menu0.setMnemonicParsing(false);
        menu0.setText("Edit");

        menuItem3.setMnemonicParsing(false);
        menuItem3.setText("Undo");
        menuItem3.setOnAction(event -> {
            System.out.println(event.toString());
            textArea.undo();
        });

        menuItem4.setMnemonicParsing(false);
        menuItem4.setText("Cut");
        menuItem4.setOnAction(event -> {
            System.out.println(event.toString());
            textArea.cut();
        });

        menuItem5.setMnemonicParsing(false);
        menuItem5.setText("Copy");
        menuItem5.setOnAction(event -> {
            System.out.println(event.toString());
            textArea.copy();
        });

        menuItem6.setMnemonicParsing(false);
        menuItem6.setText("Paste");
        menuItem6.setOnAction(event -> {
            System.out.println(event.toString());
            textArea.paste();
        });

        menuItem7.setMnemonicParsing(false);
        menuItem7.setText("Delete");
        menuItem7.setOnAction(event -> {
            System.out.println(event.toString());
            textArea.deleteText(textArea.getSelection());
    });


        menuItem8.setMnemonicParsing(false);
        menuItem8.setText("Select All");
        menuItem8.setOnAction(event -> {
            System.out.println(event.toString());
            textArea.selectAll();
        });

        menu1.setMnemonicParsing(false);
        menu1.setText("Help");

        menuItem9.setMnemonicParsing(false);
        menuItem9.setText("About Notepad");
        menuItem9.setOnAction(event -> {
            System.out.println(event.toString());
        });
        setTop(menuBar);

        BorderPane.setAlignment(textArea, javafx.geometry.Pos.CENTER);
        textArea.setPrefHeight(200.0);
        textArea.setPrefWidth(200.0);
        setCenter(textArea);

        menu.getItems().add(menuItem);
        menu.getItems().add(menuItem0);
        menu.getItems().add(menuItem1);
        menu.getItems().add(menuItem2);
        menuBar.getMenus().add(menu);
        menu0.getItems().add(menuItem3);
        menu0.getItems().add(menuItem4);
        menu0.getItems().add(menuItem5);
        menu0.getItems().add(menuItem6);
        menu0.getItems().add(menuItem7);
        menu0.getItems().add(menuItem8);
        menuBar.getMenus().add(menu0);
        menu1.getItems().add(menuItem9);
        menuBar.getMenus().add(menu1);

    }
}
