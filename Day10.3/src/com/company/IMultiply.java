package com.company;

public interface IMultiply {
    int multiply(int x, int y);
}
