public class ComplexNumber {
    private int realNumber;
    private int imgNumber;

    public ComplexNumber(){
        this.realNumber=0;
        this.imgNumber=0;
    }
    public ComplexNumber(int realNumber, int imgNumber) {
        this.realNumber = realNumber;
        this.imgNumber = imgNumber;
    }

    public ComplexNumber addComplex(ComplexNumber c){
        int realNumber = this.realNumber + c.getRealNumber();
        int imgNumber = this.imgNumber + c.getImgNumber();
        return new ComplexNumber(realNumber,imgNumber);
    }
     public ComplexNumber subtractComplex(ComplexNumber c){
        int realNumber = this.realNumber - c.getRealNumber();
        int imgNumber = this.imgNumber - c.getImgNumber();
        return new ComplexNumber(realNumber,imgNumber);
    }


    public int getRealNumber() {
        return realNumber;
    }

    public void setRealNumber(int realNumber) {
        this.realNumber = realNumber;
    }

    public int getImgNumber() {
        return imgNumber;
    }

    public void setImgNumber(int imgNumber) {
        this.imgNumber = imgNumber;
    }
}


