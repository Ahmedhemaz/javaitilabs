import java.applet.Applet;
import java.awt.Graphics;

public class HelloApplet extends Applet{
    public void paint(Graphics g){
    g.drawString("Hello", 50, 100);
    String myFont   = getParameter("font");
    g.drawString(myFont,50,150);
    }
}