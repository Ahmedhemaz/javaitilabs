import java.awt.Component;
import java.awt.*;
import java.awt.event.*;  
import java.applet.Applet;
import java.util.*; 

public class Counter extends Applet{
    Button incrementButton,decrementButton;
    String msg = "Click Count is: ";
    int counter = 0;
    public void init(){

        incrementButton = new Button("Increment");
        decrementButton = new Button("Decrement");
        incrementButton.addActionListener(new MyButtonListener());
        decrementButton.addActionListener(new MyButtonListener());
        add(incrementButton);
        add(decrementButton);


    }
    public void paint(Graphics g){
     g.drawString("Click Count is "+ counter, 50, 100);
    }

    class MyButtonListener implements ActionListener{
            @Override
            public void actionPerformed(ActionEvent e){
                if (e.getSource() == incrementButton){
                    counter++;
                    repaint();
                }
                if (e.getSource() == decrementButton){
                    counter--;
                    repaint();
                }
            }
    }
}
    

