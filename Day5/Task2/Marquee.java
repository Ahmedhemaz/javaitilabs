import java.awt.*;
import java.applet.Applet;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.image.ImageObserver;
import java.util.*; 

public class Marquee extends Applet{
    private String msg = "Java";
    private int appletWidth ;
    private int appletHeight;
    private int stringXCoordinate;
    private int stringYCoordinate;
    public void init(){
        addKeyListener(new MyArrowKeyListener());
        stringXCoordinate = 0;
        stringYCoordinate=100;
    }
    public void paint(Graphics g){
        g.drawString(msg, stringXCoordinate, stringYCoordinate);
        appletWidth = getWidth();
        appletHeight = getHeight();
    }
    
    class MyArrowKeyListener extends KeyAdapter{
        public void keyPressed(KeyEvent e){
            switch(e.getKeyCode()){
                case KeyEvent.VK_RIGHT:
                    if(stringXCoordinate < appletWidth-30){
                        stringXCoordinate+=5;
                    }
                    break;
                case KeyEvent.VK_LEFT:
                    if(stringXCoordinate > 0){
                        stringXCoordinate-=5;
                    }
                    break;
                case KeyEvent.VK_UP:
                    if(stringYCoordinate > 10){
                        stringYCoordinate-=5;
                    }
                    break;
                case KeyEvent.VK_DOWN:
                    if(stringYCoordinate < appletHeight-30){
                        stringYCoordinate+=5;        
                    }
                    break;
            }
            repaint();
        }
    }
}
