import java.awt.*;  
import java.applet.*;  
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;


public class BouncingBall extends Applet{   
    private int appletWidth;      // appletWidth
    private int appletHeight;     // appletHeight
    private int ovalXCoordinate;  // xCoordinate of oval
    private int ovalYCoordinate;  // yCoordinate of oval
    private boolean dragable;       // flag of drage
    private int ovalWidth;        // oval width
    private int ovalHeight;       // oval height
    private int cursorXCoordinate;
    private int cursorYCoordinate;


    public void init(){
        MouseKeys m = new MouseKeys();
        this.addMouseListener(m);
		this.addMouseMotionListener(m);
        ovalHeight = ovalWidth = 30;
    }

    public void paint(Graphics g){
        g.setColor(Color.yellow);
        g.fillOval(cursorXCoordinate,cursorYCoordinate,ovalHeight,ovalWidth);

    }

    public class MouseKeys extends MouseAdapter {

        @Override 
        public void mousePressed(MouseEvent e){
            ovalXCoordinate = e.getX();
            ovalYCoordinate = e.getY();
            cursorXCoordinate = ovalXCoordinate;
            cursorYCoordinate = ovalYCoordinate;
            repaint();
            dragable = true;
        }

        @Override
        	public void mouseDragged(MouseEvent e){
                if(dragable){
                     ovalXCoordinate = e.getX();
                     ovalYCoordinate = e.getY();
                     cursorXCoordinate = ovalXCoordinate;
                     cursorYCoordinate = ovalYCoordinate;
                    repaint();
                }
            }
        @Override
           public void mouseReleased(MouseEvent e)
			{
				dragable = false;
			} 

    }
        
    
}
