import java.awt.*;
import java.applet.Applet;
import java.awt.image.ImageObserver;
import java.util.*; 
import java.awt.Component;
import java.awt.event.*;  



public class BouncingBall extends Applet implements Runnable{
    private Thread th;              
    private int appletWidth;      // appletWidth
    private int appletHeight;     // appletHeight
    private int ovalXCoordinate;  // xCoordinate of oval
    private int ovalYCoordinate;  // yCoordinate of oval
    private boolean xCoordinateFlag;  // flag to indicate movement direction of x axis
    private boolean yCoordinateFlag;  // flag to indicate movement direction of x axis
    private int ovalWidth;        // oval width
    private int ovalHeight;       // oval height
    private Button start;
    private Button pause;

    public void init(){
        start = new Button("Start");
        pause = new Button("Pause");
        start.addActionListener(new MyButtonListener());
        pause.addActionListener(new MyButtonListener());
        add(start);
        add(pause);
        ovalXCoordinate = 100;
        ovalYCoordinate = 20;
        ovalHeight = ovalWidth = 30;
        th = new Thread(this);
        th.start();
    }

    public void paint(Graphics g){
        appletWidth = getWidth();
        appletHeight = getHeight();
        g.setColor(Color.yellow);
        g.fillOval(ovalXCoordinate,ovalYCoordinate,ovalHeight,ovalWidth);

    }

        class MyButtonListener implements ActionListener{
            @Override
            public void actionPerformed(ActionEvent e){
                if (e.getSource() == start){
                   th.resume();
                }
                if (e.getSource() == pause){
                    th.suspend();
                  
                }
            }
    }
    
    public void run(){
        while(true){
            if(xCoordinateFlag==true){
                ovalXCoordinate++;
                if(ovalXCoordinate==appletWidth-ovalWidth){
                    xCoordinateFlag =false;
                }
            }

            if(xCoordinateFlag==false){
                ovalXCoordinate--;
                if(ovalXCoordinate==0){
                    xCoordinateFlag =true;
                }
            }

             if(yCoordinateFlag==true){
                ovalYCoordinate++;
                if(ovalYCoordinate==appletHeight-ovalWidth){
                    yCoordinateFlag =false;
                }
            }

            if(yCoordinateFlag==false){
                ovalYCoordinate--;
                if(ovalYCoordinate==0){
                    yCoordinateFlag =true;
                }
            }

            repaint();
                try{
                    Thread.sleep(100);
                }catch(InterruptedException e){
                    e.printStackTrace();
            }  
        }
    }
}
