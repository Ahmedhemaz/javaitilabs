import java.awt.*;  
import java.applet.*;  
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;


public class DrawLine extends Applet{   
    private int appletWidth;      // appletWidth
    private int appletHeight;     // appletHeight
    private int xCoordinate;      // xCoordinate 
    private int yCoordinate;      // yCoordinate 
    private int cursorXCoordinate;
    private int cursorYCoordinate;


    public void init(){
        MouseKeys m = new MouseKeys();
        this.addMouseListener(m);
		this.addMouseMotionListener(m);
    }

    public void paint(Graphics g){
        g.setColor(Color.black);
        g.drawLine(xCoordinate,yCoordinate,cursorXCoordinate,cursorYCoordinate);
    }

    public class MouseKeys extends MouseAdapter {

        @Override 
        public void mousePressed(MouseEvent e){

            xCoordinate = e.getX();
            yCoordinate = e.getY();
            cursorXCoordinate = xCoordinate;
            cursorYCoordinate = yCoordinate;
            repaint();
        }

        @Override
        	public void mouseDragged(MouseEvent e){
                cursorXCoordinate = e.getX();
                cursorYCoordinate = e.getY();
                repaint();
            }
        @Override
           public void mouseReleased(MouseEvent e)
			{
			} 

    }
        
    
}
