import java.util.ArrayList;
import java.awt.*;  
import java.applet.*;  
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;


public class DrawLines extends Applet{

    private class Line{
        int lx1, lx2, ly1, ly2;
        public Line(int x1, int x2, int y1, int y2) {
            this.lx1 = x1;
            this.lx2 = x2;
            this.ly1 = y1;
            this.ly2 = y2;
        }
    }

    private int x1;
    private int y1;
    private int x2;
    private int y2;
    ArrayList<Line> linesArray = new ArrayList<Line>();

    public void init(){
        MouseKeys m = new MouseKeys();
        this.addMouseListener(m);
        this.addMouseMotionListener(m);
    }

    public void paint(Graphics g){
        for (Line l: linesArray) {
            g.setColor(Color.black);
            g.drawLine(l.lx1, l.ly1,l.lx2,l.ly2);
        }
        g.setColor(Color.black);
        g.drawLine(x1,y1,x2,y2);

    }

    public class MouseKeys extends MouseAdapter {

        @Override
        public void mousePressed(MouseEvent e){
            x1 = e.getX();
            y1 = e.getY();
            x2 = x1;
            y2 = y1;
        }

        @Override
        public void mouseDragged(MouseEvent e){
            x2=e.getX();
            y2=e.getY();
            repaint();
        }
        @Override
        public void mouseReleased(MouseEvent e)
        {
            //x2 = e.getX();
            //y2 = e.getY();
            linesArray.add(new Line(x1,x2,y1,y2));
            repaint();
 
        }

    }


}
