package com.company;

import java.util.Arrays;
import java.util.List;
import static java.util.stream.Collectors.toList;

public class Main {

    static List<Dish> menu = Arrays.asList(
            new Dish("pork",false,800,Dish.Type.MEAT),
            new Dish("beef",false,700,Dish.Type.MEAT),
            new Dish("chicken",false,400,Dish.Type.MEAT),
            new Dish("french fries",true,530,Dish.Type.OTHER),
            new Dish("rice",true,350, Dish.Type.OTHER),
            new Dish("season fruite",true,120, Dish.Type.OTHER),
            new Dish("pizza",true,500, Dish.Type.OTHER),
            new Dish("prawns",true,550, Dish.Type.FISH),
            new Dish("salmon",false,450, Dish.Type.FISH)
    );
    public static void main(String[] args) {

        List<Dish> vegetarianDishes =
                menu.stream()
                        .filter(Dish::isVegetarian)
                        .collect(toList());

        for (Dish d:vegetarianDishes
             ) {
            System.out.println(d.toString());

        }

        List<String> threeHighCalaorieDishNames = menu.stream().filter(d -> d.getCalories() >300)
                .map(Dish::getName)
                .limit(3)
                .collect(toList());

        threeHighCalaorieDishNames.forEach(System.out::println);

        for (String d1: threeHighCalaorieDishNames
             ) {
            System.out.println(d1);

        }
    }
}
