import java.awt.*;
import java.applet.Applet;
import java.awt.image.ImageObserver;
import java.util.*; 

public class Marquee extends Applet implements Runnable{
    private Thread th; 
    private String msg = "Java";
    private int appletWidth ;
    private int stringXCoordinate;
    private int stringYCoordinate;
    public void init(){
        stringXCoordinate = 0;
        stringYCoordinate=100;
        th = new Thread(this);
        th.start();
    }
    public void paint(Graphics g){
        g.drawString(msg, stringXCoordinate, stringYCoordinate);
        appletWidth = getWidth();
    }
    
    public void run(){
        while(true){
            stringXCoordinate++;  
            if(stringXCoordinate > appletWidth) {
                stringXCoordinate = 0;
            }
            repaint();
                try{
                Thread.sleep(30); 
            }catch(InterruptedException e){
                e.printStackTrace();
            }  
        }
    }
}
