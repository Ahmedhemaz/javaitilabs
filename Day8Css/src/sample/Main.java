package sample;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.layout.StackPane;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception{
        Text helloWorld = new Text("Hello World");
        helloWorld.setId("helloWorld");
        StackPane root = new StackPane();
        root.getChildren().add(helloWorld);
        primaryStage.setTitle("Hello World");
        Scene scene = new Scene(root,300,250);
        scene.getStylesheets().add(getClass().getResource("Style.css").toString());
        primaryStage.setScene(scene);
        primaryStage.show();
    }


    public static void main(String[] args) {
        launch(args);
    }
}
