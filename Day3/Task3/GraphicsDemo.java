import java.applet.Applet;  
import java.awt.*;  
public class GraphicsDemo extends Applet{  
    public void paint(Graphics g){
        g.setColor(Color.yellow);
        g.fillOval(100,49,150,30);
        g.fillOval(90,94,30,50);
        g.fillOval(150,84,40,70);
        g.fillOval(220,94,30,50);
        g.setColor(Color.black);
        g.drawOval(220,94,30,50);
        g.drawOval(150,84,40,70);
        g.drawOval(90,94,30,50);
        g.drawOval(100,50,150,30);
        g.drawLine(249,62,284,150);
        g.drawLine(101,62,65,150);
        g.drawLine(249,62,284,150);
        g.drawArc(65,138,219,30,180,180);
        g.drawLine(160,168,150,220);
        g.drawLine(180,168,190,220);
        g.drawRect(90,220,150,20);    
    }  
}  