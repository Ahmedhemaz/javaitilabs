import java.awt.*;
import java.applet.Applet;
import java.awt.image.ImageObserver;

public class ImageScale extends Applet
{
   Image im;

   public void init ()
   {
      im = getImage (getDocumentBase (), "Java.jpg"); 
   }

    public void paint(Graphics g) {
      
          int width = im.getWidth (this);
          int height = im.getHeight (this);
          int appletWidth = getWidth();
          int appletHeight = getHeight();

         g.drawImage (im,0,0,appletWidth,appletHeight,null);
      
    }
}