import java.awt.*;
import java.applet.Applet;
import java.awt.image.ImageObserver;

public class ShowFonts extends Applet{
    String[] s;

    public void init(){
        
        s = Toolkit.getDefaultToolkit().getFontList();
    }

 public void paint(Graphics g) {
     for(int i=0; i<s.length;i++){
         Font f = new Font(s[i],i,10);
         g.setFont(f);
         g.drawString(s[i],0,(i*10));
         
     }
    }


}

