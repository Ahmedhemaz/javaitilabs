package sample;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Group;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.effect.Reflection;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.paint.CycleMethod;
import javafx.scene.paint.LinearGradient;
import javafx.scene.paint.Stop;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class Main extends Application {

    Stop[] stops = new Stop[] {new Stop(0,Color.BLACK),new Stop(1,Color.RED)};
    LinearGradient lg1 = new LinearGradient(0,0,1,0,true, CycleMethod.REFLECT,stops);


    @Override
    public void init() throws Exception {
        String name = Thread.currentThread().getName();
        System.out.println("init() method: current Thread is: "+ name);
        super.init();
    }

    @Override
    public void start(Stage primaryStage) throws Exception{
        String name = Thread.currentThread().getName();
        System.out.println("Start() method: current Thread is: "+ name);
        //Creating a Text object
        Text text = new Text();

        //Setting font to the text
        text.setFont(Font.font(null, FontWeight.BOLD, 40));

        //setting the position of the text
        text.setX(60);
        text.setY(150);

        //Setting the text to be embedded.
        text.setText("Hello World");

        //Setting the color of the text
        text.setFill(Color.RED);

        //Instanting the reflection class
        Reflection reflection = new Reflection();

        //setting the bottom opacity of the reflection
        reflection.setBottomOpacity(0.0);

        //Applying reflection effect to the text
        text.setEffect(reflection);

        //Creating a Group object
        Group root = new Group(text);

        //Creating a scene object
        Scene scene = new Scene(root, 400, 300);

        //Setting title to the Stage
        primaryStage.setTitle("Hello World");

        //Adding scene to the stage
        primaryStage.setScene(scene);

        //Displaying the contents of the stage
        primaryStage.show();
    }

    @Override
    public void stop() throws Exception {
        String name = Thread.currentThread().getName();
        System.out.println("Stop() method: current Thread is: "+ name);
        super.stop();
    }




    public static void main(String[] args) {
        launch(args);
    }
}
