
public class Main {

    public static void main(String[] args) {
        printShape(25);
        Integer.parseInt(args[0]);
    }

    public static int printShape(int lineNumber){
        for(int i=0;i<=lineNumber;i++){
            for (int j=0;j<i;j++){
                System.out.printf("*");
            }
            for(int space=0;space<(lineNumber-i);space++)
            {
                System.out.print("  ");
            }
            for(int k=0;k<=i;k++)
            {
                System.out.print(" *");
            }
            System.out.println();
        }
        return lineNumber;
    }
    public static void printSecondShape(int lineNumber, int xCoordinateShift){
        for(int i=0;i<lineNumber;i++)
        {
            for(int space=0;space<(lineNumber-i);space++)
            {
                System.out.print(" ");
            }
            for(int j=0;j<=i;j++)
            {
                System.out.print(" *");
            }
            System.out.println();
        }
    }
}
