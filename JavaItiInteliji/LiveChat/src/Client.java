import java.io.DataInputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.net.Socket;

public class Client
{
    Socket mySocket;
    DataInputStream dis ;
    PrintStream ps;
    public static void main(String[] args)
    {
        new Client("127.0.0.1",5005);
    }
    public Client(String ip, int port)
    {
        try
        {
           Connection connection = new Connection(ip,port);
           connection.ps.println("Hello From client");
            String replyMsg = connection.dis.readLine();
            System.out.println(replyMsg);
        }
        catch(IOException ex)
        {
            ex.printStackTrace();
        }
    }
}